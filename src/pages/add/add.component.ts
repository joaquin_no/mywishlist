import { Component, OnInit } from '@angular/core';
import { ListItem, List } from '../../app/models';
import { AlertController, NavController } from 'ionic-angular';
import { WishListService } from '../../app/services/wishlist.service';

@Component({
    selector: 'app-add',
    templateUrl: 'add.component.html'
})

export class AddComponent implements OnInit {

    listName: string;
    itemName: string = "";

    items: ListItem[] = [];

    constructor(public alertController: AlertController, public navController: NavController, public wishListService: WishListService) { }

    ngOnInit() { }

    add() {
        if (this.itemName.length === 0) {
            return;
        }
        let item = new ListItem();
        item.name = this.itemName;

        this.items.push(item);
        this.itemName = "";

    }

    deleteItem(idx: number) {
        this.items.splice(idx, 1);
    }

    saveList() {
        if (this.listName.length == 0) {
            let alert = this.alertController.create({
                title: 'List Name',
                subTitle: 'The list name cannot be empty',
                buttons: ['OK']
            });
            alert.present();
            return;
        }

        let list = new List(this.listName);
        list.items = this.items;

        this.wishListService.addList(list);
        this.navController.pop();
    }
}
