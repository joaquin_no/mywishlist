import { Component } from '@angular/core';

import { PendingsComponent } from '../pending/pendings.component';
import { FinishedsComponent } from '../finished/finisheds.component';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PendingsComponent;
  tab2Root = FinishedsComponent;

  constructor() {

  }
}
