import { Component, OnInit } from '@angular/core';
import { WishListService } from '../../app/services/wishlist.service';
import { NavController } from "ionic-angular";
import { AddComponent } from '../add/add.component';

@Component({
    selector: 'pendings',
    templateUrl: 'pendings.component.html'
})

export class PendingsComponent implements OnInit {
    constructor( private wishList: WishListService, private navCtrl: NavController ) { }

    goToAdd() {
        this.navCtrl.push(AddComponent);
    }
    ngOnInit() { }
}